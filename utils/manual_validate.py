#!/usr/bin/python3
from sys import argv, exit

from redis import StrictRedis

database = redis.StrictRedis(
    host="redis", port=6379, password="", decode_responses=True)


def main(agv):
    if(agv[0] == '-r' or agv[0] == '--remove'):
        for user in agv[1:]:
            database.delete("user_"+str(user))
            print(f"user with id {agv[1]} removed from database.")
    elif(agv[0] == '-a' or agv[0] == '--add'):
        for user in agv[1:]:
            database.set("user_"+str(user), 'trusted_user')
            print(f"user with id {agv[1]} added to trusted user in database.")
    else:
        print("""
to help enter -h or --help switch at next script name.
-a  --add    to add new user to trusted users. you must to use numbric user id.
             Example: script.py -a [user_id]
             script.py -a 21342345
             script.py --add 21342345

-r  --remove    to remove a user from trusted users. you must to use numbric user id.
             Example: script.py -r [user_id]
             script.py -r 21342345
             script.py --remove 21342345
        """)
        exit(3)


if __name__ == "__main__":
    main(argv[1:])
