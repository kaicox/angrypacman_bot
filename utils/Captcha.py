#!/usr/bin/python3
from random import choice, randint, random
from string import ascii_letters, digits

from cv2 import blur, imwrite
from numpy import array, uint8, zeros
from PIL import Image, ImageDraw, ImageFont


def generate_captcha_text():
    length = randint(5, 6)
    captcha_text = "".join(choice(
        ascii_letters + digits) for _ in range(length))
    return captcha_text


def generate_captcha_picture():
    captcha_text = generate_captcha_text()

    img = zeros(shape=(60, 120, 3), dtype=uint8)
    x_pt1 = choice(range(int(img.shape[1]/3)))
    x_pt2 = choice(range(int(2*img.shape[1]/3), img.shape[1]))
    y_pt1 = choice(range(int(img.shape[0])))
    if y_pt1 > (2*img.shape[0]/3):
        y_pt2 = choice(range(int(img.shape[0]/3)))
    elif y_pt1 > (img.shape[0]/3) and y_pt1 < (2*img.shape[0]/3):
        y_pt2 = choice(
            range((int(img.shape[0]/3)), int(2*img.shape[0]/3)))
    elif y_pt1 < (img.shape[0]/3):
        y_pt2 = choice(range(int(2*img.shape[0]/3), img.shape[0]))
    else:
        y_pt2 = choice(
            range((int(img.shape[0]/3)), int(2*img.shape[0]/3)))
    img_pil = Image.fromarray(img)

    draw = ImageDraw.Draw(img_pil)
    font = ImageFont.truetype(
        font='templates/fonts/sizable-quantity/Sizable_Quantity.ttf', size=30)

    draw.text((5, 10), captcha_text, font=font, fill=(randint(
        0, 255), randint(0, 255), randint(0, 255)))
    draw.line([(x_pt1, y_pt1), (x_pt2, y_pt2)], width=3, fill=(
        randint(0, 255), randint(0, 255), randint(0, 255)))

    img = array(img_pil)

    thresh = 0.05
    for i in range(img.shape[0]):
        for j in range(img.shape[1]):
            rdn = random()
            if rdn < thresh:
                img[i][j] = 0
            elif rdn > 1-thresh:
                img[i][j] = 250

    # img_blurred = blur(img,(2,2))
    filename = "temporary/captcha"+str(randint(10000, 20000))+str(
        randint(10000, 20000))+str(randint(10000, 20000))+".jpg"
    imwrite(filename, img)

    return captcha_text, filename
