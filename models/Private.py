#!/usr/bin/python3
from pyrogram.types import (InlineKeyboardButton, InlineKeyboardMarkup,
                            ReplyKeyboardMarkup, ReplyKeyboardRemove)


def prepare_captcha(app, message, database):
    if(database.get("user_"+str(message.chat.id)) == 'trusted_user'):
        app.send_message(chat_id=message.chat.id,
                         text="شما کاربر مورد اعتماد ما هستید.")
    elif(message.command[1:]):
        chat_id, user_id = str(message.command[1:][0]).split('_')
        chat_id = int(chat_id)
        user_id = int(user_id)

        if(message.from_user.id != user_id):
            message.reply(
                "شما نمیتوانید برای دیگر کاربران اعتمادسازی انجام دهید.")
        else:
            app.send_message(chat_id=message.chat.id, text="میخوای آخرین پیامی که در گروه برایت پاک کردم رو اینجا بفرستم برات تا اگه طولانی بود یا حجم زیادی رو آپلود کرده بودی دوباره فرستادنش اذیتت نکنه؟", reply_markup=InlineKeyboardMarkup([
                [
                    InlineKeyboardButton(
                        text="آره", callback_data="1@send_messege"),
                    InlineKeyboardButton(
                        text="نه", callback_data="0@send_messege")
                ]
            ]))
            if(database.get("user_"+str(message.from_user.id)+"_validate_keyboard")):
                app.delete_messages(chat_id=int(database.get("user_"+str(message.from_user.id)+"_validate_keyboard").split(
                    "_")[0]), message_ids=int(database.get("user_"+str(message.from_user.id)+"_validate_keyboard").split("_")[1]))
                database.delete(
                    "user_"+str(message.from_user.id)+"_validate_keyboard")
    else:
        app.send_message(chat_id=message.chat.id,
                         text="شما حتما باید از طریق گروه وارد ربات شوید و آن را استارت نمایید.")


def hidden_command_pv(app, message, database):
    if(message.text and message.text == 'luntrustmel'):
        database.delete("user_"+str(message.from_user.id))
        message.reply("اعتماد شما از دست رفت. :(")
