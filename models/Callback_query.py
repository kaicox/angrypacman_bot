#!/usr/bin/python
from codecs import decode as decd
from json import loads as jloads
from os import remove
from pickle import loads
from random import randint

from pyrogram.types import (InlineKeyboardButton, InlineKeyboardMarkup,
                            InputMediaPhoto)
from utils.Captcha import generate_captcha_picture, generate_captcha_text

rules = """
✔️ #قوانین گروه کاربران آرچ لینوکس



🔳 هدف از  گروه چیست؟
▪️هدف از ایجاد گروه بحث و تبادل نظر ، سوال و جواب در زمینه توزیع آرچ است. (پس از فوروارد کردن محتواهای تبلیغاتی، پیام‌های نامرتبط خودداری کنید.


🔳 همچنین:
▪️تبادل نظر، اطلاعات، دانش و تجربه در خصوص دیگر توزیع‌های گنولینوکس، برنامه نویسی، شبکه، وب مستری، iot و کلن موضوعات دنیای تکنولوژی
▪️اطلاع رسانی رویدادها، کارگاه ها و دوره‌های آموزشی
▪️معرفی کتاب‌ها و آموزش‌های مرتبط
▪️قرار دادن لینک آموزش‌ها در وبلاگ‌های شخصی و یا فوروارد کردن مطالب مرتبط با موضوع گروه از کانال‌های دیگر مشکلی ندارد. 


🔳 ما در این گروه:
▪️ تکه‌تکه پیام نمی‌دهیم و پیام‌هایمان را در یک قسمت ارسال می‌کنیم.
▪️ تبلیغات (کمپین ها، جذب مخاطب و...) درج نمی‌کنیم مگر اینکه بطور مستقیم بنفع اعضای گروه باشد.
▪️ عکس، فیلم و لینک «بدون توضیح» منتشر نمی‌کنیم. دست کم ذکر می‌کنیم که کاربرانِ مخاطب این پیام چه کسانی هستند و بدرد چه کسانی می‌خورد.
▪️اینجا گروه کاربران است، بحث‌های خارج از موضوع گروه (مانند موسیقی، فیلم و...) تا اندازه‌ای که به چالش تبدیل نشده و کوتاه باشد، مجاز است، اما در صورت طولانی شدن و تغییر روند گروه، مجاز نمی‌باشد.


🔳 چند نکته مهم:
▪️توجه داشته باشید که پیام های تبریک، انگیزشی، بی احترامی، اخبار سیاسی، بحث‌های حاشیه‌ای و چت های بی مورد، #اسپم محسوب شده و با عرض شرمندگی حذف خواهند شد. (در صورت تکرار یا بی توجهی، ارسال کننده محدود میشود)
▪️از بحث های دو نفره و طولانی در گروه جداً خودداری کنید. برای این منظور به خصوصی خودتان مراجعه کنید. (از بحث‌های طولانی بی‌ربط به موضوع گروه خودداری کنید.)
▪️سعی کنید سوال‌های تخصصی خود را در انجمن آرچ bbs.archusrs.ir مطرح کنید و برای بحث درمورد سوال لینک کوتاه (در قالب متن) آن را در گروه ارسال کنید.
▪️زبان گروه فارسی است، پس لطفن فینگلیش ننویسید.
▪️گروه توسط ربات جلوی اسپمر‌ها رو میگیره، اگر کسی بی‌دلیل Restrict شد به یکی از ادمین‌ها برای پیگیری پیام بده.
▪️از ارسال لاگ‌ها و سورس‌های طولانی در یک پیام خودداری کنید و برای کدهای برنامه نویسی از یک سرویس مثل https://tio.run و برای ارسال لاگ‌ها از یک سرویس مثل https://dpaste.de استفاده کنید.


این قوانین صرفاً جهت افزایش سطح کیفی گروه‌ است. اگر پیشنهاد یا انتقادی دارید، محبت کنید با ما در میان بگذارید. مدیریت و حفظ این قوانین با کمک شما امکان پذیر خواهد بود.


مشاهده قوانین:
@ArchLinux_IR

کانال آرچ‌ لینوکس پارسی:
@ArchNewsFA

انجمن آرچ لینوکس پارسی:
bbs.archusers.ir

ویکی آرچ لینوکس پارسی:
wiki.archusers.ir
"""


def generate_captcha_picture_and_text(app, callback_query, database):
    captcha_password, captcha_photo_filename = generate_captcha_picture()
    group_id = str(database.get(
        "user_"+str(callback_query.from_user.id)).split("_")[0])
    database.set("user_"+str(callback_query.from_user.id),
                 group_id+'_'+str(captcha_password))

    key_possibility = randint(1, 4)
    if(key_possibility == 1):
        button1 = captcha_password
        button2 = generate_captcha_text()
        button3 = generate_captcha_text()
        button4 = generate_captcha_text()
    elif(key_possibility == 2):
        button1 = generate_captcha_text()
        button2 = captcha_password
        button3 = generate_captcha_text()
        button4 = generate_captcha_text()
    elif(key_possibility == 3):
        button1 = generate_captcha_text()
        button2 = generate_captcha_text()
        button3 = captcha_password
        button4 = generate_captcha_text()
    elif(key_possibility == 4):
        button1 = generate_captcha_text()
        button2 = generate_captcha_text()
        button3 = generate_captcha_text()
        button4 = captcha_password

    return button1, button2, button3, button4, captcha_photo_filename


def send_captcha(app, callback_query, edit, database):
    if(edit == 0):
        button1, button2, button3, button4, captcha_photo_filename = generate_captcha_picture_and_text(
            app, callback_query, database)
        app.answer_callback_query(
            callback_query.id, text="شما دوبار بیشتر فرصت ندارید.")
        app.delete_messages(chat_id=callback_query.message.chat.id,
                            message_ids=callback_query.message.message_id)

        app.send_photo(chat_id=callback_query.message.chat.id, photo=captcha_photo_filename, caption="لطفا شبیه ترین جواب به تصویر بالا را انتخاب کنید.", reply_markup=InlineKeyboardMarkup([
            [
                InlineKeyboardButton(
                    text="عوض کردن کپچا(فقط یکبار)", callback_data="recaptcha")
            ],
            [
                InlineKeyboardButton(
                    text=button1, callback_data=button1+"@answer1"),
                InlineKeyboardButton(
                    text=button2, callback_data=button2+"@answer1")

            ],
            [
                InlineKeyboardButton(
                    text=button3, callback_data=button3+"@answer1"),
                InlineKeyboardButton(
                    text=button4, callback_data=button4+"@answer1")
            ]

        ]))
        remove(captcha_photo_filename)
    else:
        button1, button2, button3, button4, captcha_photo_filename = generate_captcha_picture_and_text(
            app, callback_query, database)
        app.answer_callback_query(callback_query.id, text="captcha تغییر کرد.")

        app.edit_message_media(chat_id=callback_query.message.chat.id, message_id=callback_query.message.message_id, media=InputMediaPhoto(media=captcha_photo_filename, caption="این آخرین مهلت برای پاسخ دادن است."), reply_markup=InlineKeyboardMarkup([
            [
                InlineKeyboardButton(
                    text=button1, callback_data=button1+"@answer2"),
                InlineKeyboardButton(
                    text=button2, callback_data=button2+"@answer2")

            ],
            [
                InlineKeyboardButton(
                    text=button3, callback_data=button3+"@answer2"),
                InlineKeyboardButton(
                    text=button4, callback_data=button4+"@answer2")
            ]

        ]))
        remove(captcha_photo_filename)


def send_last_msg(app, chat_id, message):
    message = jloads(message)
    if("text" in message):
        app.send_message(chat_id=chat_id, text=message["text"])
    else:
        if("voice" in message):
            if(not "caption" in message):
                app.send_voice(chat_id=chat_id,
                               voice=message["voice"]["file_id"])
            else:
                app.send_voice(
                    chat_id=chat_id, voice=message["voice"]["file_id"], caption=message["caption"])
        elif("photo" in message):
            if(not "caption" in message):
                app.send_photo(chat_id=chat_id,
                               photo=message["photo"]["file_id"])
            else:
                app.send_photo(
                    chat_id=chat_id, photo=message["photo"]["file_id"], caption=message["caption"])
        elif("document" in message):
            if(not "caption" in message):
                app.send_document(
                    chat_id=chat_id, document=message["document"]["file_id"])
            else:
                app.send_document(
                    chat_id=chat_id, document=message["document"]["file_id"], caption=message["caption"])
        elif("video" in message):
            if(not "caption" in message):
                app.send_video(chat_id=chat_id,
                               video=message["video"]["file_id"])
            else:
                app.send_video(
                    chat_id=chat_id, video=message["video"]["file_id"], caption=message["caption"])
        elif("sticker" in message):
            if(not "caption" in message):
                app.send_sticker(
                    chat_id=chat_id, sticker=message["sticker"]["file_id"])
            else:
                app.send_sticker(
                    chat_id=chat_id, sticker=message["sticker"]["file_id"], caption=message["caption"])
        elif("animation" in message):
            if(not "caption" in message):
                # , unsave=True)
                app.send_animation(
                    chat_id=chat_id, animation=message["animation"]["file_id"])
            else:
                # , unsave=True)
                app.send_animation(
                    chat_id=chat_id, animation=message["animation"]["file_id"], caption=message["caption"])
        elif("audio" in message):
            if(not "caption" in message):
                app.send_audio(chat_id=chat_id,
                               audio=message["audio"]["file_id"])
            else:
                app.send_audio(
                    chat_id=chat_id, audio=message["audio"]["file_id"], caption=message["caption"])
        elif("video_note" in message):
            if(not "caption" in message):
                app.send_video_note(
                    chat_id=chat_id, video_note=message["video_note"]["file_id"])
            else:
                app.send_video_note(
                    chat_id=chat_id, video_note=message["video_note"]["file_id"], caption=message["caption"].marvideo_note)
        elif("contact" in message):
            if(not "caption" in message):
                app.send_contact(chat_id=chat_id, phone_number=message["contact"]["phone_number"], first_name=message[
                                 "contact"]["first_name"], last_name=message["contact"]["last_name"], vcart=message["contact"]["vcart"])
            else:
                app.send_contact(chat_id=chat_id, phone_number=message["contact"]["phone_number"], first_name=message["contact"]
                                 ["first_name"], last_name=message["contact"]["last_name"], vcart=message["contact"]["vcart"], caption=message["caption"])
        elif("location" in message):
            if(not "caption" in message):
                app.send_location(
                    chat_id=chat_id, latitude=message["location"]["latitude"], longitude=message["location"]["longitude"])
            else:
                app.send_location(chat_id=chat_id, latitude=message["location"]["latitude"],
                                  longitude=message["location"]["longitude"], caption=message["caption"])
        else:
            app.send_message(
                chat_id=chat_id, text="من نوانستم پیام شمارا تشخیص دهم.")


def callback_query_captcha(app, callback_query, database):
    if((callback_query.data == "1@send_messege") or (callback_query.data == "0@send_messege")):
        if(str(callback_query.data.split('@')[0]) == "1"):
            app.answer_callback_query(callback_query.id, text="باشه میفرستم.")
            app.delete_messages(chat_id=callback_query.from_user.id,
                                message_ids=callback_query.message.message_id)
            pickled_msg = database.get(
                "user_"+str(callback_query.from_user.id)+"_last_message")
            temp_msg = loads(decd((pickled_msg).encode(), "base64"))
            send_last_msg(app, callback_query.from_user.id, temp_msg)

            app.send_message(chat_id=callback_query.message.chat.id, text="توجه کنید که یک سوال از شما پرسیده میشود که اگر دوبار جواب اشتباه بدهید، مجبورید 5 دقیقه صبر کنید و در گروه مجدد پیام بدید تا بتوانید دوباره اعتمادسازی انجام دهید.", reply_markup=InlineKeyboardMarkup([
                [
                    InlineKeyboardButton(
                        text="من آماده ام بیا شروع کنیم", callback_data="lets_begin")
                ]
            ]))

        else:
            app.answer_callback_query(
                callback_query.id, text="باشه میندازمش دور.")
            database.delete(
                "user_"+str(callback_query.from_user.id)+"_last_message")
            app.edit_message_text(chat_id=callback_query.message.chat.id, message_id=callback_query.message.message_id, text="توجه کنید که یک سوال از شما پرسیده میشود که اگر دوبار جواب اشتباه بدهید، مجبورید 5 دقیقه صبر کنید و در گروه مجدد پیام بدید تا بتوانید دوباره اعتمادسازی انجام دهید.", reply_markup=InlineKeyboardMarkup([
                [
                    InlineKeyboardButton(
                        text="من آماده ام بیا شروع کنیم", callback_data="lets_begin")
                ]
            ]))
    elif(callback_query.data == "lets_begin"):
        send_captcha(app, callback_query, 0, database)

    elif(callback_query.data == "recaptcha"):
        send_captcha(app, callback_query, 1, database)

    elif(callback_query.data.split('@')[1] == "answer1"):
        true_answer = database.get(
            "user_"+str(callback_query.from_user.id)).split("_")[1]
        if(callback_query.data.split('@')[0] == true_answer):
            app.answer_callback_query(callback_query.id, text="پاسخ شما صحیح است و در حال حاضر محدودیتی در گروه ندارید." +
                                      "\nلطفا قوانین گروه را مطالعه فرمایید.", show_alert=True)
            app.send_message(chat_id=callback_query.from_user.id, text=rules)
            app.delete_messages(chat_id=callback_query.message.chat.id,
                                message_ids=callback_query.message.message_id)
            group_id = int(
                str(database.get("user_"+str(callback_query.from_user.id)).split("_")[0]))
            database.set(
                "user_"+str(callback_query.message.chat.id), 'trusted_user')
            try:
                app.unban_chat_member(
                    chat_id=group_id, user_id=callback_query.from_user.id)
            except:
                pass
        else:
            app.answer_callback_query(
                callback_query.id, text="پاسخ شما اشتباه است، تنها یک فرصت برای شما باقی ست لطفا دقت کنید.", show_alert=True)
            send_captcha(app, callback_query, 1, database)

    elif(callback_query.data.split('@')[1] == "answer2"):
        true_answer = database.get(
            "user_"+str(callback_query.from_user.id)).split("_")[1]
        if(callback_query.data.split('@')[0] == true_answer):
            app.answer_callback_query(callback_query.id, text="پاسخ شما صحیح است و در حال حاضر محدودیتی در گروه ندارید." +
                                      "\nلطفا قوانین گروه را مطالعه فرمایید.", show_alert=True)
            app.send_message(chat_id=callback_query.from_user.id, text=rules)
            app.delete_messages(chat_id=callback_query.message.chat.id,
                                message_ids=callback_query.message.message_id)
            group_id = int(
                str(database.get("user_"+str(callback_query.from_user.id)).split("_")[0]))
            database.set(
                "user_"+str(callback_query.message.chat.id), 'trusted_user')
            try:
                app.unban_chat_member(
                    chat_id=group_id, user_id=callback_query.from_user.id)
            except:
                pass
        else:
            app.answer_callback_query(
                callback_query.id, text="پاسخ شما اشتباه است، شما آخرین فرصتان را از دست دادید لطفا 5 دقیقه دیگر امتحان کنید و یا از ادمین ها بخواید تا کمکتان کنند.", show_alert=True)
            app.delete_messages(chat_id=callback_query.message.chat.id,
                                message_ids=callback_query.message.message_id)
    else:
        app.answer_callback_query(
            callback_query.id, text="نمیتوانم کمکی بکنم.")
