#!/usr/bin/python
from codecs import encode as encd
from pickle import dumps
from re import search
from time import sleep, time

from pyrogram.types import (ChatPermissions, InlineKeyboardButton,
                            InlineKeyboardMarkup)


def validate_keyboard(app, message, database):
    if(database.get("user_"+str(message.from_user.id)+"_validate_keyboard") is None):
        app.restrict_chat_member(chat_id=message.chat.id, user_id=message.from_user.id,
                                 permissions=ChatPermissions(), until_date=int(time() + 300))

        trustme_inline = app.send_message(chat_id=message.chat.id, text=f"""
درود [{message.from_user.first_name}](tg://user?id={message.from_user.id}) عزیز، شما برای ارسال پیام در این گروه باید اعتمادسازی انجام دهید.
        """, reply_markup=InlineKeyboardMarkup([
            [
                InlineKeyboardButton(
                    text="انجام اعتمادسازی در ربات گروه", url=f"https://t.me/{str(app.get_me()['username'])}?start={message.chat.id}_{message.from_user.id}")
            ]
        ]))
        database.set("user_"+str(message.from_user.id),
                     str(message.chat.id)+"_D3am7P")
        database.set("user_"+str(message.from_user.id)+"_validate_keyboard",
                     str(trustme_inline.chat.id)+"_"+str(trustme_inline.message_id), ex=300)
        try:
            sleep(300)
            trustme_inline.delete()
        except:
            pass


def check_validation(app, message, database):
    user_validate_value = database.get("user_"+str(message.from_user.id))
    if(user_validate_value != 'trusted_user'):
        temp_msg = message
        message.delete()
        pickled_msg = str(encd(dumps(str(temp_msg)), "base64").decode())
        database.set("user_"+str(message.from_user.id) +
                     "_last_message", pickled_msg, ex=300)
        validate_keyboard(app, message, database)


def remove_robot(app, message, database):
    temp_message = message
    temp_message.delete()
    for new_chat_member in message.new_chat_members:
        if(database.get("user_"+str(new_chat_member.id)) != 'trusted_user'):
            if(new_chat_member.is_bot):
                kick_message = app.kick_chat_member(chat_id=message.chat.id,
                                                    user_id=new_chat_member.id)
                kick_message.delete()
                try:
                    app.restrict_chat_member(chat_id=message.chat.id, user_id=message.from_user.id,
                                             permissions=ChatPermissions(can_send_messages=True))
                except:
                    pass


def admin_aid(app, message, database):
    command = (message.text[1:])[:2]
    if(command == 'qa'):
        try:
            (message.reply_to_message).reply(
                "لطفا سوال اصلی خود را به صورت کامل در یک پست ارسال کنید درصورتی که کسی بتواند به شما کمک کند شما را ریپلای میکند.")
        except:
            pass
        message.delete()
    elif(command == 'ln'):
        chat_link = app.export_chat_invite_link(message.chat.id)
        if(message.reply_to_message):
            (message.reply_to_message).reply(f"[لینک گروه]({chat_link})")
        else:
            message.reply(f"[لینک گروه]({chat_link})")
        message.delete()
    elif(command == 'ch'):
        try:
            if(database.get("user_"+str(message.reply_to_message.from_user.id)) == 'trusted_user'):
                message.reply(
                    f"کاربر موردنظر با نام {message.reply_to_message.from_user.first_name} قابل اعتماد است.")
            else:
                message.reply(
                    f"کاربر موردنظر با نام {message.reply_to_message.from_user.first_name} قابل اعتماد نمیباشد.")
        except:
            pass
        message.delete()
    elif(command == 'ut'):
        database.delete("user_"+str(message.reply_to_message.from_user.id))
        (message.reply_to_message).reply(
            "شما دیگر قابل اعتماد نیستید، لطفا فرآیند اعتمادسازی را مجدد انجام دهید.")
        message.delete()
    elif(command == 'tr'):
        for entity in message.reply_to_message.entities:
            database.set(
                "user_"+str(entity.user.id), 'trusted_user')
            app.unban_chat_member(chat_id=message.chat.id,
                                  user_id=entity.user.id)
            validate_message_id = (database.get(
                "user_"+str(entity.user.id)+"_validate_keyboard")).split("_")[1]
            app.delete_messages(chat_id=message.chat.id,
                                message_ids=int(validate_message_id))
            database.delete(
                "user_"+str(entity.user.id)+"_validate_keyboard")
            app.send_message(
                chat_id=message.chat.id, text=f"یکی از ادمین ها وساطت [{entity.user.first_name}](tg://user?id={entity.user.id}) را کرده است.")
            message.delete()
            break
    elif(command == 'hp'):
        message.reply(""" 
دستورات ربات که فقط برای ادمین ها است به شرح زیر است:
>qa کمک کردن به کاربران برای بهتر سوال کردن.
(کاربر را ریپلای کنید و آن را بفرستید.)

>ln برای دریافت لینک گروه است.

>ch با این دستور میتوان فهمید که کاربر موردنظر مورد اعتماد است یا خیر.
(باید کاربر موردنظر را ریپلای کنید.)

>ut با این دستور میتوان کاربران را سلب اعتماد کرد.
(باید کاربر موردنظر ریپلای شود.)

>tr  با این دستور میتوان فرآیند اعتمادسازی را برای کاربران تسهیل کرد.
(باید پیام ربات را که برای اعتمادسازی ارسال کرده است را ریپلای کنید.)
        """)
