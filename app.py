#!/usr/bin/python3
from pyrogram import Client, filters
from redis import StrictRedis
from models.Callback_query import callback_query_captcha
from models.Private import prepare_captcha, hidden_command_pv
from models.Group import admin_aid, check_validation, remove_robot

app = Client("AngryPacman", config_file=".config.ini")
database = StrictRedis(host="redis", port=6379, password="", decode_responses=True)

is_administrator = filters.create(func=lambda _, __, msg: bool(msg._client.get_chat_member(
    msg.chat.id, msg.from_user.id).status in ('administrator', 'creator')), name="AdminFilter")


@ app.on_message(filters.group & filters.service & ~filters.pinned_message & ~filters.new_chat_members)
def delete_services_message(app, message):
    message.delete()


@ app.on_message(filters.group & filters.service & filters.new_chat_members)
def remove_new_robots(app, message):
    remove_robot(app, message, database)


@ app.on_message(filters.group & filters.text & filters.regex("^\>.*$") & is_administrator)
def aid_for_admins(app, message):
    admin_aid(app, message, database)


@ app.on_message(filters.group & ~filters.service & ~filters.edited & ~is_administrator)
def check_validate(app, message):
    check_validation(app, message, database)


@ app.on_message(filters.private & filters.command(['start']))
def private_prepare_captcha(app, message):
    prepare_captcha(app, message, database)


@ app.on_message(filters.private & filters.regex("^l.*l$"))
def hidden_command(app, message):
    hidden_command_pv(app, message, database)


@ app.on_callback_query()
def callback(app, callback_query):
    callback_query_captcha(app, callback_query, database)


if __name__ == "__main__":
    app.run()
