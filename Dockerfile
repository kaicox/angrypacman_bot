FROM python:3.8.6-slim-buster

RUN apt-get update ; \
	apt-get upgrade -y ; \
	apt-get install -y python3 python3-dev python3-pip libgl1-mesa-glx ; \
	python3 -m pip install --upgrade pip

COPY . /app

WORKDIR /app

RUN python3 -m pip install -r requirements.txt

CMD ["python3", "app.py"]
